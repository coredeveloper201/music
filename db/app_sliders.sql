-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 06, 2018 at 11:18 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smubu_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_sliders`
--

CREATE TABLE `app_sliders` (
  `id` int(11) NOT NULL,
  `slider_name` varchar(255) NOT NULL,
  `type` enum('artist','track','album','external','playlist') NOT NULL DEFAULT 'artist',
  `value` varchar(255) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_sliders`
--

INSERT INTO `app_sliders` (`id`, `slider_name`, `type`, `value`, `created_at`, `updated_at`) VALUES
(1, 'img7887.jpg', 'track', '30732', '2017-07-20 12:42:59', '2018-07-02 11:44:36'),
(2, 'img211.jpg', 'track', '31102', '2017-07-20 12:42:59', '2018-07-02 11:44:36'),
(3, 'img2561.jpg', 'track', '30433', '2017-07-20 12:42:59', '2018-07-02 11:44:36'),
(10, 'img5264.jpg', 'track', '14134', '2017-07-20 12:42:59', '2018-07-02 11:44:36'),
(11, 'img8648.jpg', 'track', '14027', '2017-07-20 12:42:59', '2018-07-02 11:44:36'),
(12, 'img266.jpg', 'track', '14336', '2017-07-20 12:42:59', '2018-07-02 11:44:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_sliders`
--
ALTER TABLE `app_sliders`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_sliders`
--
ALTER TABLE `app_sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
