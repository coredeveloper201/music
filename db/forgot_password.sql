-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 06, 2018 at 11:26 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smubu_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `forgot_password`
--

CREATE TABLE `forgot_password` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `code` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `forgot_password`
--

INSERT INTO `forgot_password` (`id`, `user_id`, `code`) VALUES
(18, 126, 'fVoaWMhppl'),
(19, 126, 'Fg3O3jpt9Z'),
(20, 126, 'I5QTAeZHts'),
(21, 1179, 'NrsL0Mm4Ec'),
(22, 3476, 'XVwCAv2sxW'),
(23, 3358, 'sYFutNPfiD'),
(24, 5830, '3EB30MWipL'),
(25, 5277, 'piSd6QBume'),
(27, 5335, '3y4qnuWR0g'),
(28, 5830, 'R5oyPHK0F6'),
(29, 5830, 'j3oVMzc7y7'),
(30, 5830, 'Atxt3dN4K3'),
(31, 5830, 'utzBUlGBBn'),
(32, 5830, 'PtBtIUNOqM'),
(33, 5830, 'LD9WT5XL1v'),
(34, 5830, 'lvCreDYgW5'),
(35, 5830, '2iPSuO8xbN'),
(36, 5830, '07hTUYGCBu'),
(37, 5830, 'NKi8XHI0Te'),
(38, 5830, 'n2RUEsxi38'),
(39, 5830, 'YdAMQ4JG3c'),
(40, 5830, 'PeN7Tatm2S'),
(41, 5277, 'oxot1ul6hV'),
(49, 287, 'SXuomZF6w6'),
(50, 10418, 'INsO2W2uki'),
(51, 8813, 'EP69KGGSvr'),
(52, 13303, 'fo1B0AVFFN'),
(53, 13303, '3lglsGTj2W'),
(54, 16373, 'M9f5PSZAK7'),
(55, 16373, 'wdsYL2NHXq');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `forgot_password`
--
ALTER TABLE `forgot_password`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `forgot_password`
--
ALTER TABLE `forgot_password`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=56;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
