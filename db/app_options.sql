-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 06, 2018 at 11:17 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smubu_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_options`
--

CREATE TABLE `app_options` (
  `id` int(11) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `option_key` text NOT NULL,
  `option_value` text,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_options`
--

INSERT INTO `app_options` (`id`, `title`, `option_key`, `option_value`, `created_at`, `updated_at`) VALUES
(4, 'Discover More Artists', 'top_artists', '[\"20\",\"31\",\"50\",\"52\",\"75\",\"118\",\"134\",\"177\",\"227\",\"231\",\"359\",\"378\",\"6060\"]', '2017-07-22 07:59:16', '2018-07-05 14:13:18'),
(5, 'Top Albums', 'home_albums', '[\"75\",\"73\",\"61\",\"52\"]', '2017-07-22 07:59:25', '2017-10-25 06:08:39'),
(6, 'Bongo Fleva', 'home_genres', '[\"afrobeat\",\"Afro Beat\"]', '2017-07-22 07:59:34', '2018-07-05 08:58:11'),
(7, 'Top Trending Songs', 'home_tracks', '[\"8473\",\"13360\",\"7549\",\"7947\",\"14027\",\"15532\",\"13857\",\"8520\",\"13425\",\"7790\",\"14336\",\"4145\",\"3305\",\"8466\",\"7874\",\"9584\",\"2420\",\"1352\",\"18836\",\"23608\"]', '2017-07-22 07:59:59', '2018-05-03 13:05:14'),
(8, 'Top 24 hour songs', 'home_top_24', '[\"8520\",\"8473\",\"8466\",\"7979\",\"7874\",\"7792\",\"7791\",\"7549\",\"7197\",\"6784\",\"4909\",\"3860\",\"2434\",\"1134\",\"18449\",\"18553\",\"3266\"]', '2017-08-26 09:52:27', '2018-04-06 12:01:18'),
(9, 'We Recommend', 'we_recommended', '[\"3940\",\"3915\",\"3305\",\"3281\",\"3090\",\"3000\",\"2421\",\"1843\",\"343\",\"27\"]', '2017-08-26 11:58:16', '2017-11-25 11:39:37'),
(10, 'Just For You', 'just_for_you', NULL, '2017-08-26 11:59:26', '2017-08-26 11:59:26'),
(11, 'Play List', 'play_list', '[\"2506\",\"2420\",\"2418\",\"1881\",\"1352\",\"1285\",\"1134\",\"1124\",\"1094\",\"1053\",\"1048\",\"1045\",\"1044\",\"748\",\"744\",\"423\",\"69\",\"27\"]', '2017-09-21 06:58:45', '2017-10-14 07:30:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_options`
--
ALTER TABLE `app_options`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_options`
--
ALTER TABLE `app_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
