-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 06, 2018 at 11:21 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smubu_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `tid` int(11) NOT NULL,
  `message` text COLLATE utf8_unicode_ci NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `uid`, `tid`, `message`, `time`) VALUES
(1, 249, 1492, 'Gam music to di globe', '2017-09-04 12:36:32'),
(2, 322, 1968, 'Dope song ', '2017-10-03 15:57:27'),
(3, 1024, 3305, 'Nice', '2017-11-15 08:49:46'),
(4, 9621, 27205, 'massive ', '2018-05-29 05:43:47'),
(5, 10953, 28223, 'Its Cool', '2018-07-29 16:13:59'),
(6, 11170, 17563, 'Wonderful!', '2018-08-05 18:03:00'),
(7, 11385, 29788, 'best', '2018-08-10 19:57:09'),
(8, 11427, 36488, 'Truly entertaining and transcending all generations.', '2018-08-12 11:07:12'),
(9, 11427, 36481, 'Any attachments aside, this song promoted girl child education as many girls opted to be educated like Alice B Anyango.', '2018-08-12 11:12:53'),
(10, 12169, 41910, 'Good music Good Work ', '2018-08-27 16:06:11'),
(11, 12502, 15178, 'I sent this message out we have true people out there but their hearts BR Shields', '2018-09-03 02:12:15'),
(12, 11932, 3305, 'Fantastic ', '2018-09-06 17:27:02'),
(13, 13544, 44601, 'its a good song, and an inspirational ', '2018-09-18 18:25:17'),
(14, 15497, 33202, 'Nyimbo poa', '2018-10-16 03:57:31'),
(15, 16334, 28572, 'good song bravo', '2018-10-27 19:27:24'),
(16, 16334, 5129, 'This a good song ', '2018-10-28 08:39:49'),
(17, 16404, 65412, 'Good work', '2018-10-28 16:50:57'),
(18, 16905, 12119, 'haha yea ', '2018-11-02 18:52:50');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
