-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 06, 2018 at 11:19 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smubu_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `app_users`
--

CREATE TABLE `app_users` (
  `id` int(11) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `last_name` varchar(50) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` longtext NOT NULL,
  `user_type` enum('user','admin') NOT NULL DEFAULT 'user',
  `remember_token` longtext,
  `isActive` enum('Active','InActive') NOT NULL DEFAULT 'Active',
  `isDeleted` enum('Yes','No') NOT NULL DEFAULT 'No',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `app_users`
--

INSERT INTO `app_users` (`id`, `first_name`, `last_name`, `email`, `password`, `user_type`, `remember_token`, `isActive`, `isDeleted`, `created_at`, `updated_at`) VALUES
(1, 'CORPORATION', 'JIT', 'jad.a@jitofficial1989123879612311.com', '$2y$10$d8u6cJuA.6tJtWenNCrFq.g7F2WvZj3qD5YvGlrXko1xPDI5Qmud.', 'admin', 'iDRSTpVqGHLuMBOVG3GP3hbjS15wGbTQlUrapO3wcGp7OBYMC9mUl40Kseof', 'Active', 'No', '2017-07-20 12:29:16', '2017-11-28 14:56:43'),
(2, 'User', 'SMUBU', 'playlistdashboard@sm35u6fr6ica935271956231.com ', '$2y$10$d8u6cJuA.6tJtWenNCrFq.g7F2WvZj3qD5YvGlrXko1xPDI5Qmud.', 'user', 'Cdirag1AEamAl8XRZnRGt9qcb2uuNS1ujuGXmFTmzKHbB65a9dcf530UBv5S', 'Active', 'No', '2017-07-20 12:29:16', '0000-00-00 00:00:00'),
(3, 'CORPORATION', 'JIT', 'parimal@xsquaretec.com', '$2y$10$9gzOhO9.Kq9eS9yQ90jqD.dB8F2nOfX4QC2/KBnfB9wM/ZW7/L9Py', 'admin', 'iDRSTpVqGHLuMBOVG3GP3hbjS15wGbTQlUrapO3wcGp7OBYMC9mUl40Kseof', 'Active', 'No', '2017-07-20 12:29:16', '2017-11-28 14:56:43');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `app_users`
--
ALTER TABLE `app_users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `app_users`
--
ALTER TABLE `app_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
