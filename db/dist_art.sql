-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 06, 2018 at 11:22 AM
-- Server version: 5.6.38
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smubu_main`
--

-- --------------------------------------------------------

--
-- Table structure for table `dist_art`
--

CREATE TABLE `dist_art` (
  `id` int(11) NOT NULL,
  `d_id` int(11) NOT NULL,
  `a_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `dist_art`
--

INSERT INTO `dist_art` (`id`, `d_id`, `a_id`, `created_at`) VALUES
(1, 87, 90, '2017-07-08 09:58:15'),
(2, 0, 91, '2017-07-08 13:07:18'),
(3, 0, 92, '2017-07-08 13:07:57'),
(4, 92, 93, '2017-07-08 13:08:54'),
(5, 0, 94, '2017-07-08 14:35:58'),
(6, 96, 97, '2017-07-08 14:48:16'),
(7, 87, 122, '2017-07-17 12:27:09');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `dist_art`
--
ALTER TABLE `dist_art`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `dist_art`
--
ALTER TABLE `dist_art`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
