<?php
function PageMain() {
	global $TMPL, $LNG, $CONF, $db, $loggedIn, $settings;

	if(isset($_SESSION['username']) && isset($_SESSION['password']) || isset($_COOKIE['username']) && isset($_COOKIE['password'])) {	
		$verify = $loggedIn->verify();
		
		if(empty($verify['username'])) {
			// If fake cookies are set, or they are set wrong, delete everything and redirect to home-page
			$loggedIn->logOut();
			header("Location: ".$CONF['url']."/index.php?a=welcome");
		} else {
			// Start displaying the Feed
			$feed = new feed();
			$feed->db = $db;
			$feed->url = $CONF['url'];
			$feed->user = $verify;
			$feed->id = $verify['idu'];
			$feed->username = $verify['username'];
			$feed->per_page = $settings['perpage'];
			$feed->categories = $feed->getCategories();
			$feed->time = $settings['time'];
			$feed->c_per_page = $settings['cperpage'];
			$feed->c_start = 0;
			$feed->l_per_post = $settings['lperpost'];
			$feed->paypalapp = $settings['paypalapp'];
			$feed->online_time = $settings['conline'];
			$feed->friends_online = $settings['ronline'];
			$feed->subscriptionsList = $feed->getSubs($verify['idu'], 0);
			$feed->trackList = implode(',', $feed->getTrackList(((!empty($feed->profile_id)) ? $feed->profile_id : $feed->id)));
			$feed->updateStatus($verify['offline']);
			
			// Useed in timeline javascript which checks for new messages
			$TMPL['subs'] = 1;
			
			$TMPL_old = $TMPL; $TMPL = array();
			$skin = new skin('artists/manage_artist'); $rows = '';
			
			if(empty($_GET['filter'])) {
				$_GET['filter'] = '';
			}
			// Allowed types
			list($timeline, $message) = $feed->stream(0, $_GET['filter']);
			
			$artists = $feed->artistInDistibutor($verify['idu']);

			$TMPL['add_artists'] = $CONF['url'].'/index.php?a=manage_artists&mode=add';
			$TMPL['artists_in'] = $artists['string'];
			$TMPL['artists_count'] = $artists['count'];

			$TMPL['messages'] = $timeline;

			if( isset($_GET['mode']) && !empty($_GET['mode']) && ( $_GET['mode'] == 'add' ) ){
				$skin = new skin('artists/add_artist'); $rows = '';
				$TMPL['all_distributors_url'] = $CONF['url'].'/index.php?a=manage_artists';

				if( isset($_POST['add_artist']) && !empty($_POST['add_artist']) ){
					
						unset($_POST['add_artist']);

						$strMessage = '<div class="notification-box notification-box-error">';

						$isError = false;
						foreach ($_POST as $key => $value) {
							if(empty($value)){
								$isError = true;
								$strMessage .= "<p>".ucwords($key)." is required.</p><br/>";
							}
						}

						$strMessage .= '<div class="notification-close notification-close-error"></div></div>';

						if(!$isError){
							$manageDistributor = new manageDistributor();
							$manageDistributor->db = $db;							
							$isExists 	= $manageDistributor->checkEmailExistsOrNot($_POST['email']);

							if(!$isExists){
								global $settings;

								$arrData['first_name'] 				= $_POST['first_name'];
								$arrData['last_name'] 				= $_POST['last_name'];
								$arrData['username'] 				= $_POST['username'];
								$arrData['password'] 				= $_POST['password'];
								$arrData['email'] 					= $_POST['email'];
								$arrData['phone'] 					= $_POST['phone'];

								$arrData['like_notification'] 		= $settings['notificationl'];
								$arrData['comment_notification'] 	= $settings['notificationc'];
								$arrData['chat_notification'] 		= $settings['notificationd'];
								$arrData['friend_notification'] 	= $settings['notificationf'];
								$arrData['email_like'] 				= $settings['email_like'];
								$arrData['email_comment'] 			= $settings['email_comment'];
								$arrData['email_new_friend'] 		= $settings['email_new_friend'];
								$arrData['accounts_per_ip'] 		= $settings['aperip'];


								$reg = new register();
								$reg->db = $db;

								$reg->artistSIgnUpByDist($arrData,$verify['idu']);
								header("Location: ".$CONF['url']."/index.php?a=manage_artists");
							}else{
								$strMessage = '<div class="notification-box notification-box-error">';
								$strMessage .= '<p>Email id already exists</p>';
								$strMessage .= '<div class="notification-close notification-close-error"></div></div>';

								$TMPL['message'] = $strMessage;
							}
						}else{

							$TMPL['message'] = $strMessage;	
						}
				}
			}

			if( isset($_GET['mode']) && !empty($_GET['mode']) && ( $_GET['mode'] == 'edit' ) ){
				$artistId = $_GET['id'];
				$skin = new skin('artists/edit_artist'); $rows = '';
				$TMPL['all_distributors_url'] = $CONF['url'].'/index.php?a=manage_artists';
				$artist = $feed->getArtistById($artistId);

				$TMPL['first_name'] = $artist['first_name'];
				$TMPL['last_name'] = $artist['last_name'];
				$TMPL['email'] = $artist['email'];
				$TMPL['phone'] = $artist['phone'];

				if( isset( $_POST['update_artist'] ) && !empty( $_POST['update_artist'] ) ){
						
					unset($_POST['update_artist']);

					$strMessage = '<div class="notification-box notification-box-error">';

					$isError = false;
					foreach ($_POST as $key => $value) {
						if(empty($value)){
							$isError = true;
							$strMessage .= "<p>".ucwords($key)." is required.</p><br/>";
						}
					}

					$strMessage .= '<div class="notification-close notification-close-error"></div></div>';

					if(!$isError){

						$arrData['first_name'] = $_POST['first_name'];
						$arrData['last_name'] = $_POST['last_name'];
						$arrData['email'] = $_POST['email'];
						$arrData['phone'] = $_POST['phone'];

						$reg = new register();
						$reg->db = $db;

						$reg->updateartistByDist($arrData,$artistId);

						header("Location: ".$CONF['url']."/index.php?a=manage_artists");
					}else{
						$TMPL['message'] = $strMessage;	
					}

					
				}
			}
			$rows = $skin->make();
			
			
			$TMPL = $TMPL_old; unset($TMPL_old);
			$TMPL['rows'] = $rows;
		}
	} else {
		// If the session or cookies are not set, redirect to home-page
		header("Location: ".permalink($CONF['url']."/index.php?a=welcome"));
	}
	
	if(isset($_GET['logout']) == 1 && $_GET['token_id'] == $_SESSION['token_id']) {
		$loggedIn->logOut();
		header("Location: ".permalink($CONF['url']."/index.php?a=welcome"));
	}

	$TMPL['url'] = $CONF['url'];
	$date = explode('-', wordwrap($_GET['filter'], 4, '-', true));
	$month = intval($date[1]);
	$TMPL['title'] = $LNG['stream'].(!empty($_GET['filter']) ? ' - '.$LNG["month_{$month}"].' '.$date[0].' - ' : ' - ').$settings['title'];
	// $TMPL['header'] = pageHeader($LNG['stream']);

	$skin = new skin('shared/content');
	return $skin->make();
}
?>