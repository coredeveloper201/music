<?php
function PageMain() {
	global $TMPL, $LNG, $CONF, $db, $loggedIn, $settings;


	if(isset($_SESSION['username']) && isset($_SESSION['password']) || isset($_COOKIE['username']) && isset($_COOKIE['password'])) {

		$verify = $loggedIn->verify();

		if($verify['username']) {
			if($verify['type'] != "user"){
				header("Location: ".permalink($CONF['url']."/index.php?a=stream"));
			}
		}
	}



    // Start the music feed
	$feed = new feed();
	$feed->db = $db;
	$feed->url = $CONF['url'];
	$feed->user = $verify;
	$feed->id = $verify['idu'];
	$feed->username = $verify['username'];
	$feed->per_page = $settings['perpage'];
	$feed->categories = $feed->getCategories();
	$feed->time = $settings['time'];
	$feed->l_per_post = $settings['lperpost'];

	// Get the popular tracks
	$result = $db->query("SELECT `views`.`track`,`tracks`.`title`,`tracks`.`art`, COUNT(`track`) as `count` FROM `views`,`tracks` WHERE `views`.`track` = `tracks`.`id` AND DATE_SUB(CURDATE(),INTERVAL 1 DAY) <= date(`views`.`time`) AND `tracks`.`public` = 1 AND `art` != 'default.png' GROUP BY `track` ORDER BY `count` DESC LIMIT 20");


	$i = 1;
	while($row = $result->fetch_assoc()) {
		if($i > 10) {
			$popular_extra[] = $row;
		} else {
			$popular[] = $row;
		}
		$i++;
	}

	// Fetch Artists
	$result 	= $db->query("SELECT `option_value` FROM `options` WHERE `option_key` = 'top_artists'");
	$row 		= $result->fetch_assoc();
	$arrArtistId = json_decode($row['option_value']);
	$arrArtistDataOutPut = "";

	foreach ($arrArtistId as $key => $value) {
		$result 	= $db->query("SELECT * FROM `users` WHERE `idu` = '".$value."'");
		$row 		= $result->fetch_assoc();
		$arrArtistDataOutPut .= '<div class="col3 front-div text-center">';
		$arrArtistDataOutPut .= '<a rel="loadpage" href="'.$CONF['url']."/index.php?a=profile&u=".$row['username'].'"><img src="'.$CONF['url'].'/uploads/avatars/'.$row['image'].'" class="circle-img"></a>';
		$arrArtistDataOutPut .= '<a rel="loadpage" href="'.$CONF['url']."/index.php?a=profile&u=".$row['username'].'">';
		if( !empty($row['first_name']) || ( !empty($row['last_name']) ) ){
			$arrArtistDataOutPut .= '<h4>'.ucwords($row['first_name']).' '.ucwords($row['last_name']).'</h4>';
		}else{
			$arrArtistDataOutPut .= '<h4>'.ucwords($row['username']).'</h4>';
		}
		$result1 	= $db->query("SELECT COUNT(subscriber) as followers FROM `relations` WHERE `leader` = '".$value."'");
		$rows 		= $result1->fetch_assoc();
		$arrArtistDataOutPut .= '</a>';
		if($rows['followers'] == 1){
			$arrArtistDataOutPut .= '<p>'.$rows['followers'].' follower</p>';
		}else
		{
			$arrArtistDataOutPut .= '<p>'.$rows['followers'].' followers</p>';
		}
		$arrArtistDataOutPut .= '</div>';
	}
	$TMPL['top_artists'] = $arrArtistDataOutPut;
	$TMPL['top_artists'] = "";
	$TMPL['allartists'] = $CONF['url'].'/index.php?a=artists';


	// Fetch Albums
	$result 	= $db->query("SELECT `option_value` FROM `options` WHERE `option_key` = 'home_albums'");
	$row 		= $result->fetch_assoc();
	$arrAlbumsId = json_decode($row['option_value']);
	$arrAlbumsDataOutPut = "";
	foreach ($arrAlbumsId as $key => $value) {
		$query = "SELECT u.*,a.*,a.image as album_image FROM `albums` as a INNER JOIN `users` as u on a.user_id = u.idu WHERE a.id = '".$value."'";
		$result 	= $db->query($query);
		$row 		= $result->fetch_assoc();

		$arrAlbumsDataOutPut .= '<div class="col3 front-div">';
		$arrAlbumsDataOutPut .= '<a rel="loadpage" href="'.$CONF['url'].'/index.php?a=album&id='.$row['id'].'&name='.cleanurl($row['name']).'">';
		$arrAlbumsDataOutPut .=	'<img src="'.$CONF['url'].'/requests/albums/'.$row['album_image'].'">';
		$arrAlbumsDataOutPut .= '</a>';
		$arrAlbumsDataOutPut .= '<a rel="loadpage" href="'.$CONF['url'].'/index.php?a=album&id='.$row['id'].'&name='.cleanurl($row['name']).'">';
		$arrAlbumsDataOutPut .= '<h4>'.ucwords($row['name']).'</h4>';
		$arrAlbumsDataOutPut .= '</a>';
		$arrAlbumsDataOutPut .= '<a rel="loadpage" href="'.$CONF['url']."/index.php?a=profile&u=".$row['username'].'">';
		if( !empty($row['first_name']) || ( !empty($row['last_name']) ) ){
			$arrAlbumsDataOutPut .= '<p>'.ucwords($row['first_name']).' '.ucwords($row['last_name']).'</p>';
		}else{
			$arrAlbumsDataOutPut .= '<p>'.ucwords($row['username']).'</p>';
		}
		$arrAlbumsDataOutPut .= '</a>';
		$arrAlbumsDataOutPut .= '</div>';

	}
	$TMPL['top_albums'] = $arrAlbumsDataOutPut;
	$TMPL['allalbums'] = $CONF['url'].'/index.php?a=all_albums';

	// Fetch Artists
	//$result 	= $db->query("SELECT `option_value` FROM `options` WHERE `option_key` = 'home_geners'");
/*	$result 	= $db->query("SELECT `option_value` FROM `app_options` WHERE `option_key` = 'home_genres'");
	$row 		= $result->fetch_assoc();
	$arrGenreName = json_decode($row['option_value']);

	$arrGenreDataOutPut = "";

	foreach ($arrGenreName as $key => $value) {
		$arrGenreDataOutPut .= 	'<div class="page-section">';
		$arrGenreDataOutPut .= 	'<h1 class="hide-mobile">'.ucwords($value).'</h1>';

		$sql = "SELECT t.*,t.id as track_id,t.title as track_title,u.*,u.image as user_image FROM `tracks` as t INNER JOIN `users` as u ON t.uid = u.idu WHERE t.tag = '".$value."' GROUP BY t.uid LIMIT 0,6";

		$allRows = array();

		$results 	= $db->query($sql);



		while($rows = $results->fetch_assoc()){
			$allRows[] = $rows;
		}

		foreach ($allRows as $key1 => $value1) {



			$arrGenreDataOutPut .= '<div class="col3 front-div">';
			$arrGenreDataOutPut .= '<a rel="loadpage" href="'.$CONF['url'].'/index.php?a=track&id='.$value1['track_id'].'&name='.cleanurl($value1['track_title']).'"><img src="'.$CONF['url'].'/uploads/avatars/'.$value1['user_image'].'"/></a>';
			$strArtistName  = "";
			if( !empty($value1['first_name']) || !empty($value1['last_name']) ){
				$strArtistName = ucwords($value1['first_name'])." ".ucwords($value1['last_name']);
			}else{
				$strArtistName = ucwords($value1['username']);
			}
			$arrGenreDataOutPut .= '<a rel="loadpage" href="'.$CONF['url']."/index.php?a=profile&u=".$value1['username'].'"><h4>'.$strArtistName.'</h4></a>';
			$arrGenreDataOutPut .= '</div>';

		}
		$arrGenreDataOutPut .= 	'<a rel="loadpage" href="'.$CONF['url'].'/index.php?a=explore&filter='.$value.'" class="more-btn2">'.ucwords($value).' Artists <i class="fa fa-chevron-right" aria-hidden="true"></i></a>';

		$arrGenreDataOutPut .= '</div>';
	}*/

	//$TMPL['all_genre'] = $arrGenreDataOutPut;
	$TMPL['all_genre'] = "";

	$result 	= $db->query("SELECT `id`,`category` FROM `app_playlist_categories` WHERE `isActive` = 'Active' AND isDeleted = 'No' AND ID IN ( SELECT category_id FROM `home_playlist` where isActive = 'Active' AND isDeleted = 'No' GROUP BY category_id ) ORDER BY sr_no ASC");
	$arrCatId = array();
	while( $row 		= $result->fetch_assoc() ){
		$arrCatId[] = $row;
	}

	$strPlayCatOutput = "";

	if( count($arrCatId) > 0 ){

		foreach ($arrCatId as $key => $value) {

			//$strPlayCatOutput .= 	'<div class="owl-theme page-section playlist_slider'.$key.'">';
			$strPlayCatOutput .= 	'<h1 class="playlist-section-title">'.ucwords($value['category']).'</h1>';
			$strPlayCatOutput .= 	'<div class="owl-carousel owl-theme  page-section playlist_slider'.$key.'">';

			$strSQl = "SELECT id,playlist_title,playlist_image,playlist_tracks FROM home_playlist WHERE category_id = ".$value['id']." AND isActive='Active' AND isDeleted='No' ORDER BY sr_no ASC";
			$result = $db->query($strSQl);
			$arrPlaylists = array();
			while( $row 		= $result->fetch_assoc() ){
				$arrPlaylists[] = $row;
			}

			foreach ($arrPlaylists as $key1 => $value1) {

				$arrData = json_decode( $value1['playlist_tracks'] );
				$intCount = count($arrData);
				/*if( getUserIp() == "103.240.171.250" ){*/
					$strCount = '<span class="xsquare-song-count">'.$intCount.' Songs</span>';
				/*}else{
					$strCount = "";
				}*/
				$strPlayCatOutput .= '<div class="front-div item">';
				$strPlayCatOutput .= '<a rel="loadpage" href="'.$CONF['url'].'/index.php?a=playlistpage&id='.$value1['id'].'"><img src="'.$CONF['url'].'/thumb.php?src='.$value1['playlist_image'].'&t=pl&w=300&h=300"/></a>';

				/*/app-admin/public/assets/photos/playlist_images/*/
				/*https://www.smubuafrica.com/thumb.php?src=img9656.jpg&t=pl&w=300&h=300*/


				$strPlayCatOutput .= '<a rel="loadpage" href="'.$CONF['url']."/index.php?a=playlistpage&id=".$value1['id'].'"><h4>'.$value1['playlist_title'].'</h4>'.$strCount.'</a>';
				$strPlayCatOutput .= '</div>';

			}

			$strPlayCatOutput .= '</div>';
		}

	}

	$TMPL['all_playlist'] = $strPlayCatOutput;


	/*echo "<pre>";
	print_r($arrCatId);
	echo "</pre>";
	die;*/

/*	echo "<pre>";
	print_r($TMPL);
	echo "</pre>";
	die;*/

	// Fetch Tranding Songs
// Comment Start	
	$result 	= $db->query("SELECT t.*,u.* FROM `tracks` as t INNER JOIN `users` as u ON t.uid = u.idu ORDER BY views DESC LIMIT 0 , 10");

	$arrRecords = array();
	while($row 		= $result->fetch_assoc()){
		$arrRecords[] = $row;
	}
	$arrTrendingSongs = "";
/*	echo "<pre>";
	print_r($arrRecords);
	echo "</pre>";
	die;*/
	$arrTrendingSongs = $feed->trending();


	/*foreach ($arrRecords as $key => $value) {
		$arrTrendingSongs .= '<div class="treading-song">';
		$arrTrendingSongs .= '<div class="left-treading">';
		$arrTrendingSongs .= '<div class="track song-play-btn" id="play'.$value['id'].'" data-track-id="'.$value['id'].'" data-track-name="'.$value['name'].'"></div>';

		$arrTrendingSongs .= '<h4 id="song-name'.$value['id'].'">'.ucwords($value['title']).'</h4>';

		$strArtistName = "";

		if( !empty($value['first_name']) || !empty($value['last_name']) ){
			$strArtistName = ucwords($value['first_name'])." ".ucwords($value['last_name']);
		}else{
			$strArtistName = ucwords($value['username']);
		}

		$arrTrendingSongs .= '<p class="hover-artist"><a rel="loadpage" href="'.$CONF['url'].'/index.php?a=profile&amp;u='.$value['username'].'">'.$strArtistName.'</a></p>';
		$arrTrendingSongs .= '<div id="profile-card" class="artist-hover-info">';
		$arrTrendingSongs .= '<div class="profile-card-cover"><img src="'.$CONF['url'].'/uploads/covers/'.$value['cover'].'"></div>';
		$arrTrendingSongs .= '<div class="profile-card-avatar">';
		$arrTrendingSongs .= '<a rel="loadpage" href="'.$CONF['url'].'/index.php?a=profile&amp;u='.$value['username'].'"><img src="'.$CONF['url'].'/uploads/avatars/'.$value['image'].'"></a>';
		$arrTrendingSongs .= '</div>';

		$arrTrendingSongs .= '<div class="profile-card-info"><div class="profile-card-username"><a href="'.$CONF['url'].'/index.php?a=profile&amp;u='.$value['username'].'" rel="loadpage"><span id="author'.$value['idu'].$value['username'].'"></span><span id="time'.$value['idu'].$value['username'].'"></span><div class="cover-text-container">'.ucwords($value['username']).'</div></a></div><div class="profile-card-location"></div></div><div class="profile-card-buttons"><a href="http://musica.sparkenproduct.in/index.php?a=profile&amp;u='.$value['username'].'" rel="loadpage"></a></div>';



		$arrTrendingSongs .= '</div>';

		$arrTrendingSongs .= '<div class="player-controls">
		<div id="song-controls'.$value['id'].'">
			<div id="jp_container_123" class="jp-audio">

			</div>
		</div>
	</div>';
	$arrTrendingSongs .= '</div>';
	$arrTrendingSongs .= '<div class="right-treading"><span class="jp-current-time">3:58</span><ul><li><i class="fa fa-play" aria-hidden="true"></i> 3.5k</li><li><i class="fa fa-heart" aria-hidden="true"></i> 3.5k</li></ul></div>';
	$arrTrendingSongs .= '</div>';
}*/

$TMPL['trending_songs'] = $arrTrendingSongs['0'];

$TMPL['popular'] = welcomeTracks($popular, $CONF['url']);
$TMPL['popular_extra'] = $TMPL['popular'].welcomeTracks($popular_extra, $CONF['url']);

	// Get the latest tracks (excludes the tracks with no artwork set)
$result = $db->query("SELECT *, `tracks`.`id` as `track` FROM `tracks` WHERE `public` = 1 AND `art` != 'default.png' ORDER BY `id` DESC LIMIT 20");
$i = 1;
while($row = $result->fetch_assoc()) {
	if($i > 10) {
		$latest_extra[] = $row;
	} else {
		$latest[] = $row;
	}
	$i++;
}

$TMPL['latest'] = welcomeTracks($latest, $CONF['url']);
$TMPL['latest_extra'] = $TMPL['latest'].welcomeTracks($latest_extra, $CONF['url']);

	// Get the site categories
$result = $db->query("SELECT * FROM `categories` ORDER BY `name`");
while($row = $result->fetch_assoc()) {
	$tags[] = $row;
}

$TMPL['categories'] = welcomeCategories($tags, $CONF['url']);

$TMPL['url'] = $CONF['url'];

if($settings['paypalapp']) {
	$skin = new skin('welcome/gopro'); $go_pro = '';
	$go_pro = $skin->make();
}
$TMPL['go_pro'] = $go_pro;

//$TMPL['title'] = $LNG['welcome'].' - '.$settings['title'];
//$TMPL['meta_description'] = $settings['title'].' '.$LNG['welcome_about'];

$TMPL['title'] = "SMUBU - African Music - Free Mp3 Download | Audio Download or listen | Smubuafrica.com";
$TMPL['meta_description'] = "Free music download! Stream and download high quality mp3 and listen to popular playlists. Exclusively on SMUBU.";
$TMPL['ad'] = $settings['ad1'];

$skin = new skin('welcome/content');
return $skin->make();
}
?>