<?php
function PageMain() {
	global $TMPL, $LNG, $CONF, $db, $settings;
	$TMPL['url'] = $CONF['url'];
	if(isset($_POST['artist_sugnup'])){
		
		$strMessage = '<div class="notification-box notification-box-error">';

		$isError = false;
		$arrVData = $_POST;
		unset($arrVData['first_name']);
		unset($arrVData['last_name']);
		foreach ($arrVData as $key => $value) {
			if(empty($value) ){
				$isError = true;
				$strMessage .= "<p>".ucwords($key)." is required.</p><br/>";
			}
		}
		$strMessage .= '<div class="notification-close notification-close-error"></div></div>';

		if(!$isError){

			$reg = new register();
			$reg->db = $db;
			$reg->username = $_POST['username'];
			$user = $reg->checkUserName();	

			if($user){
				$strMessage = '<div class="notification-box notification-box-error">';
				$strMessage .= "<p>User Name is Already Exists.</p><br/>";	
				$strMessage .= '<div class="notification-close notification-close-error"></div></div>';
				$TMPL['message'] = $strMessage;
				$skin = new skin('artists/signup');
				return $skin->make();

			}else{
				$arrData['first_name'] 				= $_POST['first_name'];
				$arrData['last_name'] 				= $_POST['last_name'];
				$arrData['country'] 				= $_POST['country'];
				$arrData['username'] 				= $_POST['username'];
				$arrData['password'] 				= $_POST['password'];
				$arrData['email'] 					= $_POST['email'];
				$arrData['phone'] 					= $_POST['phone'];

				$arrData['like_notification'] 		= $settings['notificationl'];
				$arrData['comment_notification'] 	= $settings['notificationc'];
				$arrData['chat_notification'] 		= $settings['notificationd'];
				$arrData['friend_notification'] 	= $settings['notificationf'];
				$arrData['email_like'] 				= $settings['email_like'];
				$arrData['email_comment'] 			= $settings['email_comment'];
				$arrData['email_new_friend'] 		= $settings['email_new_friend'];
				$arrData['accounts_per_ip'] 		= $settings['aperip'];

				$reg->artistSIgnUp($arrData);
				header("Location: ".$CONF['url']);

			}
		}else{
			$TMPL['message'] = $strMessage;
			$skin = new skin('artists/signup');
			return $skin->make();
		}
		
	}
	else{
		$skin = new skin('artists/signup');
		return $skin->make();
	}
}
?>