<?php
function PageMain() {
	global $TMPL, $LNG, $CONF, $db, $loggedIn, $settings;
	

	if(isset($_SESSION['username']) && isset($_SESSION['password']) || isset($_COOKIE['username']) && isset($_COOKIE['password'])) {	
		$verify = $loggedIn->verify();
	}

	if(!isset( $_GET['id'] ) && empty( $_GET['id'] )) {
		header("Location: ".$CONF['url']."/index.php?a=welcome");
	}
	
	// Start displaying the Feed
	$feed = new feed();
	$feed->db = $db;
	$feed->url = $CONF['url'];
	$feed->user = $verify;
	$feed->id = $verify['idu'];
	$feed->username = $verify['username'];
	$feed->per_page = -1;
	$feed->categories = $feed->getCategories();
	$feed->time = $settings['time'];
	$feed->c_start = 0;
	$feed->l_per_post = 0;
	$feed->shuffle = 1;
	$TMPL_old = $TMPL; $TMPL = array();
	// Get the track
	$artists = $feed->HomeplaylistTracks($_GET['id']);
	$strPageTitle = $feed->HomeplaylistTitle($_GET['id']);

	/*echo "<pre>";	
	print_r($strPageTitle['playlist_title']);
	echo "</pre>";	
	die;*/

	// Match the content from the song-title class in order to set it for the title tag
	preg_match_all('/<div.*(class="playlist-title").*>([\d\D]*)<\/div>/iU', $artists[0], $title);
	if(empty($title[2][0])) {
		preg_match_all('/<div.*(class="page-header").*>([\d\D]*)<\/div>/iU', $artists[0], $title);
	}
	
	$TMPL['id'] = $_GET['id'];
	$TMPL['url'] = $CONF['url'];
	$TMPL['title'] = "Playlist";

	$skin = new skin('track/rows'); $rows = '';
	
	// If the output is empty redirect to home-page
	
	$TMPL['messages'] = $artists[0];
	$TMPL['page_title'] = "Playlist";
	$TMPL['page_image'] = $strPageTitle['playlist_image'];

	$rows = $skin->make();
	
	$TMPL = $TMPL_old; unset($TMPL_old);
	$TMPL['rows'] = $rows;
	$TMPL['page_title'] = $strPageTitle['playlist_title'];
	$TMPL['page_image'] = $strPageTitle['playlist_image'];
	

	$TMPL['url'] = $CONF['url'];
	
	$title = trim(strip_tags("Playlist Page"));
	$TMPL['title'] = $title.' - '.$settings['title'];
	$TMPL['meta_description'] = $title.' '.$feed->sidebarDescription($_GET['id'], 1, 1);

	$skin = new skin('playlistpage/content');
	return $skin->make();
}
?>