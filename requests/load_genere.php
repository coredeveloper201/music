﻿<?php
include("../includes/config.php");
session_start();
include("../includes/classes.php");
include(getLanguage(null, (!empty($_GET['lang']) ? $_GET['lang'] : $_COOKIE['lang']), 2));
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");

$resultSettings = $db->query(getSettings());
$settings = $resultSettings->fetch_assoc();
if(isset($_SESSION['username']) && isset($_SESSION['password'])) {
	$loggedInAdmin = new loggedIn();
	$loggedInAdmin->db = $db;
	$loggedInAdmin->url = $CONF['url'];
	$loggedInAdmin->username = $_SESSION['username'];
	$loggedInAdmin->password = $_SESSION['password'];
	$loggedIn = $loggedInAdmin->verify();

	if($loggedIn['username']) {
		$manageCategories = new manageCategories();
		$manageCategories->db = $db;
		$manageCategories->url = $CONF['url'];
		
		$cat = $manageCategories->getCategoryDropDown();
		echo $cat;
	}
}
mysqli_close($db);
?>