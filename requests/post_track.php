<?php
include("../includes/config.php");
session_start();
if($_POST['token_id'] != $_SESSION['token_id']) {
	return false;
}
include("../includes/classes.php");
require_once(getLanguage(null, (!empty($_GET['lang']) ? $_GET['lang'] : $_COOKIE['lang']), 2));
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");

$resultSettings = $db->query(getSettings()); 
$settings = $resultSettings->fetch_assoc();

// The theme complete url
$CONF['theme_url'] = $CONF['theme_path'].'/'.$settings['theme'];

// If message is not empty
if(!empty($_POST)) {
	if($_POST['error']) {
		if($_POST['desc']) {
			$err[] = array(6, 5000);
		}
		if($_POST['buy']) {
			$err[] = array(7);
		}
		if($_POST['tag_max']) {
			$err[] = array(8, 30);
		}
		if($_POST['tag_min']) {
			$err[] = array(9, 1);
		}
		if($_POST['ttl_min']) {
			$err[] = array(10);
		}
		if($_POST['ttl_max']) {
			$err[] = array(11, 100);
		}

		foreach($err as $error) {
			$message .= notificationBox('error', sprintf($LNG["{$error[0]}_upload_err"], ((isset($error[1])) ? $error[1] : ''), ((isset($error[2])) ? $error[2] : '')));
		}
		
		$update = array($message);

	} else {
/*		echo "<pre>";
		print_r($_FILES);
		print_r($_POST);
		echo "</pre>";
		die;*/
		$isError = false;
		$isUpload = true;
		
		if( is_array( $_FILES['track']['size'] ) && count( $_FILES['track']['size'] ) == 1 ){
			/*1000000*/

			if( $_FILES['track']['size']['0'] > 6200000 ){
				unset($_POST['title']['0']);
				unset($_FILES['track']['name']['0']);
				unset($_FILES['track']['type']['0']);
				unset($_FILES['track']['tmp_name']['0']);
				unset($_FILES['track']['error']['0']);
				unset($_FILES['track']['size']['0']);
				$update['0'] = '<div class="notification-box notification-box-error"><p>Track Size Should Not Be Greater Then 6 MB</p><div class="notification-close notification-close-error"></div></div>';
				$isError = true;
				$isUpload = false;
			}

		}else{
			$arrNames = array();
			for ($i=0; $i <= count( $_FILES['track']['size'] ); $i++) { 
				
				if( $_FILES['track']['size'][$i] > 6200000 ){
					$arrNames[] = $_POST['title'][$i];
					unset( $_POST['title'][$i] );
					unset( $_POST['description'][$i] );
					unset( $_POST['tag'][$i] );

					unset($_FILES['track']['name'][$i]);
					unset($_FILES['track']['type'][$i]);
					unset($_FILES['track']['tmp_name'][$i]);
					unset($_FILES['track']['error'][$i]);
					unset($_FILES['track']['size'][$i]);
					$isError = true;
				}
			}

			if( count( $_FILES['track']['name'] ) > 0 ){
				$isUpload = true;	
			}

			if( $arrNames ){
				$strNames = implode(",", $arrNames);
				$update['0'] = '<div class="notification-box notification-box-error"><p>'.$strNames.' This Tracks will be skipped because size is greater then 6 MB</p><div class="notification-close notification-close-error"></div></div>';
				$isError = true;
			}
		}
		
		if($isUpload && !$isError){
			// If the user have session or cookie set
			if(isset($_SESSION['username']) && isset($_SESSION['password']) || isset($_COOKIE['username']) && isset($_COOKIE['password'])) {
				$loggedIn = new loggedIn();
				$loggedIn->db = $db;
				$loggedIn->url = $CONF['url'];
				$loggedIn->username = (isset($_SESSION['username'])) ? $_SESSION['username'] : $_COOKIE['username'];
				$loggedIn->password = (isset($_SESSION['password'])) ? $_SESSION['password'] : $_COOKIE['password'];
				
				$verify = $loggedIn->verify();
				
				// If user is authed successfully
				if($verify['username']) {

					$feed = new feed();
					$feed->db = $db;
					$feed->url = $CONF['url'];
					$feed->user = $verify;
					$feed->id = $verify['idu'];
					$feed->username = $verify['username'];
					$feed->album_id = $verify['album'];
					$feed->per_page = $settings['perpage'];
					$feed->art_size = $settings['artsize'];
					$feed->art_format = $settings['artformat'];
					$feed->paypalapp = $settings['paypalapp'];
					$feed->track_size_total = ($feed->getProStatus($feed->id, 1) ? $settings['protracktotal'] : $settings['tracksizetotal']);
					$feed->track_size = ($feed->getProStatus($feed->id, 1) ? $settings['protracksize'] : $settings['tracksize']);
					$feed->track_format = $settings['trackformat'];
					$feed->time = $settings['time'];
					$arrSkipeedTracks = array();
					if( isset( $arrNames ) && count( $arrNames ) > 0 ){
						$arrSkipeedTracks = $arrNames;
					}
					$update = $feed->updateTrack($_POST, 1,$arrSkipeedTracks);
				}
			}	
		}
		
	}
}
echo json_encode(array("result" => (strpos($update[0], 'notification-box-error') > 0 ? 0 : 1), "message" => $update[0]));
mysqli_close($db);
?>