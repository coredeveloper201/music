﻿<?php

header("Access-Control-Allow-Origin: *");


include("../includes/config.php");
session_start();
include("../includes/classes.php");
require_once(getLanguage(null, (!empty($_GET['lang']) ? $_GET['lang'] : $_COOKIE['lang']), 2));
$db = new mysqli($CONF['host'], $CONF['user'], $CONF['pass'], $CONF['name']);
if ($db->connect_errno) {
    echo "Failed to connect to MySQL: (" . $db->connect_errno . ") " . $db->connect_error;
}
$db->set_charset("utf8");

$strEncoded =$_POST['encoded'];
$mcrypt = new MCrypt();
$strDecryptFileName = $mcrypt->decrypt($strEncoded);
echo $strDecryptFileName;
mysqli_close($db);
die;
?>